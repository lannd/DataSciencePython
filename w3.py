import numpy as np
#--------------- PART A -------------
# 1 ---------------
# all code in file hw_numpy.py
# End 1 -----------

# 2 ---------------
n = int(input())
arr = np.random.uniform(low=1, high=10, size=(n,n,n))
print(arr)
print(np.min(arr, axis=0))
print("--------------------------------------")
print(np.min(arr, axis=1))
print("--------------------------------------")
print(np.min(arr, axis=2))
print("--------------------------------------")
print(np.max(arr, axis=0))
print("--------------------------------------")
print(np.max(arr, axis=1))
print("--------------------------------------")
print(np.max(arr, axis=2))
print("--------------------------------------")
print(np.sum(arr, axis=0))
print("--------------------------------------")
print(np.sum(arr, axis=1))
print("--------------------------------------")
print(np.sum(arr, axis=2))
print("--------------------------------------")
# End 2 -----------

# 3 ---------------
m = int(input())
n = int(input())
k = int(input())
arr = np.random.randint(low=-100, high=100, size=(m, n, k))
print(arr)
print(np.min(arr, axis=0))
print("--------------------------------------")
print(np.min(arr, axis=1))
print("--------------------------------------")
print(np.min(arr, axis=2))
print("--------------------------------------")
print(np.max(arr, axis=0))
print("--------------------------------------")
print(np.max(arr, axis=1))
print("--------------------------------------")
print(np.max(arr, axis=2))
print("--------------------------------------")
print(np.sum(arr, axis=0))
print("--------------------------------------")
print(np.sum(arr, axis=1))
print("--------------------------------------")
print(np.sum(arr, axis=2))
print("--------------------------------------")
# End 3 -----------

# 4 ---------------
arr = np.random.uniform(low=-10, high=10, size=(10, 8))
print(arr)
val = float(input())
arr_temp = np.abs(arr - val)
iMin = arr_temp.argmin()
print(arr[int(iMin / 8), iMin % 8])

arr_1d = arr_temp.ravel()
arrR = np.argsort(arr_1d)[:3]
print(arr[int(arrR[0] / 8), arrR[0] % 8])
print(arr[int(arrR[1] / 8), arrR[1] % 8])
print(arr[int(arrR[2] / 8), arrR[2] % 8])
# End 4 -----------

# 5 ---------------
print(np.fv(0.05/12, 10*12, -100, -100))
a = np.array((0.05, 0.06, 0.07))/12
print(np.fv(a, 10*12, -100, -100))

print(np.pv(0.05/12, 10*12, -100, 15692.93))
a = np.array((0.05, 0.04, 0.03))/12
print(np.pv(a, 10*12, -100, 15692.93))

print(np.npv(0.281,[-100, 39, 59, 55, 20]))

print(np.pmt(0.075/12, 12*15, 200000))

principal = 2500.00
per = np.arange(1*12) + 1
ipmt = np.ipmt(0.0824/12, per, 1*12, principal)
ppmt = np.ppmt(0.0824/12, per, 1*12, principal)
pmt = np.pmt(0.0824/12, 1*12, principal)
print(np.allclose(ipmt + ppmt, pmt))

fmt = '{0:2d} {1:8.2f} {2:8.2f} {3:8.2f}'
for payment in per:
    index = payment - 1
    principal = principal + ppmt[index]
    print(fmt.format(payment, ppmt[index], ipmt[index], principal))

interestpd = np.sum(ipmt)
print(np.round(interestpd, 2))

print(round(np.irr([-100, 39, 59, 55, 20]), 5))
print(round(np.irr([-100, 0, 0, 74]), 5))
print(round(np.irr([-100, 100, 0, -7]), 5))
print(round(np.irr([-100, 100, 0, 7]), 5))
print(round(np.irr([-5, 10.5, 1, -8, 1]), 5))

print(round(np.nper(0.07/12, -150, 8000), 5))
print(np.nper(*(np.ogrid[0.07/12:0.08/12:0.01/12,-150:-99:50,8000:9001:1000])))
# End 5 -----------
#--------------- End PART A -------------

#--------------- PART B -------------

#--------------- End PART B -------------
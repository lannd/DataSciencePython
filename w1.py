import datetime

def homework_5():
    now = datetime.datetime.now()
    print("Current date and time : ")
    print(now.strftime("%Y-%m-%d %H:%M:%S"))

def homework_6():
    def sum(x, y):
        sum = x + y
        if sum in range(15, 20):
            return 20
        else:
            return sum

    print(sum(10, 6))
    print(sum(10, 2))
    print(sum(10, 12))


if __name__ == "__main__":
    homework_5()
    homework_6()

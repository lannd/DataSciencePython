import numpy as np

x = range(1, 11)
print(x)
a1 = np.array(x, 'i')
print(a1)
a2 = np.array(x, 'f')
print(a2)
#--------------------------------
arr = np.zeros((2, 3, 4))
print(arr)
arr = np.ones((2, 3, 4))
print(arr)
arr = np.arange(1000)
print(arr)
#--------------------------------
a = np.array([2, 3.2, 5.5, -6.4, -2.2, 2.4])
print(a)
print(a[1])
print(a[1:4])
a = np.array([[2, 3.2, 5.5, -6.4, -2.2, 2.4],
              [1, 22, 4, 0.1, 5.3, -9],
              [3, 1, 2.1, 21, 1.1, -2]])
print(a)
print(a[:, 3]) # duyet tat ca dong, cot 3
print(a[1:4, 0:4]) # dong 1->4, cot 0->4
print(a[1:, 2]) # dong 1->het, cot 2
#--------------------------------
arr = np.array([range(4), range(10, 14)]) # 2d
print(arr)
print(arr.shape)
print(arr.size)
print(arr.max())
print(arr.min())
#--------------------------------
print(np.reshape(arr, (2, 2, 2)))
print(np.transpose(arr)) # chuyen vi
print(np.ravel(arr))
print(arr.astype('f'))
#--------------------------------
a = np.array([range(4), range(10, 14)])
b = np.array([2, -1, 1, 0])
print(a)
print(b)
print(a * b)
b1 = b * 100
b2 = b * 100.0
print(b1, b2)
print(b1 == b2)
print(b1.dtype, b2.dtype)
#--------------------------------
arr = np.arange(10)
print(arr)
print(arr < 3)
print(np.less(arr, 3))
condition = np.logical_or(arr < 3, arr > 8)
print(condition)
new_arr = np.where(condition, arr * 5, arr * -5)
print(new_arr)
#--------------------------------
def calcMagnitude(u, v, minmag = 0.1):
    mag = ((u ** 2) + (v ** 2)) ** 0.5
    print(mag)
    output = np.where(mag > minmag, mag, minmag) # nhung thang nao lonw hon minmag thi khong doi gia tri, con nguoc lai gan bang minmag
    return output
u = np.array([[4, 5, 6], [2, 3, 4]])
v = np.array([[2, 2, 2], [1, 1, 1]])
print(calcMagnitude(u, v))
u = np.array([[4, 5, 0.01], [2, 3, 4]])
v = np.array([[2, 2, 0.03], [1, 1, 1]])
print(calcMagnitude(u, v))
#--------------------------------
import numpy.ma as MA
marr = MA.masked_array(range(10), fill_value = -999)
print(marr, marr.fill_value)
marr[2] = MA.masked
print(marr)
print(marr.mask)
narr = MA.masked_where(marr > 6, marr)
print(narr)
x = MA.filled(narr)
print(x)
print(type(x))
#--------------------------------
m1 = MA.masked_array(range(1, 9))
print(m1)
m2 = m1.reshape(2, 4)
print(m2)
m3 = MA.masked_greater(m2, 6) #mask nhung thang > 6
print(m3)
res = m3 - np.ones((2, 4))
print(res)
print(type(res))
import string
import random
import math

# ------------ Home work 1 ---------------
def hw_1(strInput):
    arrS = strInput.replace(' ', '').split(',')
    arrE = []
    arrO = []
    for item in arrS:
        num = int(item)
        if num % 2 == 0:
            arrE.append(num)
        else:
            arrO.append(num)
    return arrE, arrO

print(hw_1("12, 32, 31, 12, 42, 21, 58, 92,37,45"))
# ------------ End home work 1 ---------------



# ------------ Home work 2 ---------------
def get_sum(x):
    #return sum(x)
    return sum([ite for ite in x if type(ite) is int or type(ite) is float])

l = [(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]
l.sort(key=get_sum, reverse=False)
print(l)
# ------------ End home work 2 ---------------



# ------------ Home work 3 ---------------
s = 'the output that tells you if the object is the type you think or not'
s_1 = s.split(' ')
s_1.sort()
print(s_1)
print(' '.join(word[0].upper() + word[1:] for word in s.split()))
# ------------ End home work 3 ---------------


# ------------ Home work 4 ---------------
def max(lst):
    sizeLst = len(lst)
    if sizeLst > 0:
        indexMax = 0
        for i in range(1, sizeLst):
            if lst[indexMax] < lst[i]:
                indexMax = i
        return lst[indexMax]
    return None

def min(lst):
    sizeLst = len(lst)
    if sizeLst > 0:
        indexMin = 0
        for i in range(1, sizeLst):
            if lst[indexMin] > lst[i]:
                indexMin = i
        return lst[indexMin]
    return None

def mean(lst):
    sum = 0
    for ele in lst:
        sum += ele
    return sum / len(lst)

def countFrequency(lst):
    dic = {}
    for ele in lst:
        if ele in dic:
            dic[ele] += 1
        else:
            dic[ele] = 1
    return dic

def varianceAndStandardDeviation(dictFre, mean, coutEle):
    sTmp = 0
    for ele in dictFre.keys():
        num = int(ele)
        sTmp += dictFre[ele] * num * num
    vari = (sTmp / coutEle) - mean
    stan = math.sqrt(vari)
    return vari, stan

print("Please enter number: ")
countNum=int(input())
l = [random.randint(0, 10) for i in range(0, countNum)]
print(l)
dicRe = countFrequency(l)
for ele in dicRe.keys():
    print(str(ele) + " : " + str(dicRe[ele]))
print("Max: " + str(max(l)))
print("Min: " + str(min(l)))
meanRe = mean(l)
print("Mean: " + str(meanRe))
variRe, stanRe = varianceAndStandardDeviation(dicRe, meanRe, countNum)
print("Variance: " + str(variRe))
print("Standard deviation: " + str(stanRe))
# ------------ End home work 4 ---------------


# ------------ Home work 5 ---------------
def hw_5():
    arr = []
    for i in range(100):
        numRan = random.randint(1, 10)
        arr.append(float(numRan * numRan) / (4 * numRan))
    return arr

print(hw_5())
# ------------ End home work 5 ---------------


# ------------ Home work 6 ---------------
def sumCol(matrix, col):
    sum = 0
    for row in range(len(matrix)):
        sum += matrix[row][col]
    return sum

M = [[1,2,3,4,5],
    [3,4,2,5,6],
    [1,6,3,2,5]]
print([sumCol(M, iC) for iC in range(0, len(M[0]))])
# ------------ End home work 6 ---------------